#! /usr/bin/env bash
set -ex
rm -f *.out *.o && \
gcc -Werror -Wextra -Wall -xc++ -o main.o -c main.cpp && \
gcc -Werror -Wextra -Wall -xc++ -o tertiary.o -c tertiary.cpp && \
gcc -Werror -Wextra -Wall -shared-libgcc -o test.out main.o tertiary.o -lstdc++ && \
./test.out

