//#include <gmock/gmock.h>
//#include <gtest/gtest.h>
#include <functional>
#include <cassert>
#include <iostream>
#include "header.h"
//#include <boost/thread.hpp>

extern int global_y;
const std::string CLIENT_NAME("BUGLE");

enum foo {MODE_VALID, MODE_INVALID} spam;
struct X { int a;} BLASM;
typedef X Y;
class Flubble {
    private:
        std::string glubble = "Flubble's glubble!";
    public:
        void print() {
            std::cout << glubble << std::endl;
            std::cout << "CLIENT_NAME: " << CLIENT_NAME << std::endl;
        }

};

//TEST(BLAG, wakka) {
//    Flubble flubble;
//    flubble.print();
//}

int main() 
{
    int x = 2;
    std::cout << x << std::endl;
    //std::cout << global_y << std::endl;
    printer();
    std::cout << foo::MODE_VALID << std::endl;
    std::cout << foo::MODE_INVALID << std::endl;
    spam = foo::MODE_INVALID;
    std::cout << "spam is: " << spam << std::endl;
    BLASM.a = 3;
    std::cout << "BLASM.a is: " << BLASM.a << std::endl;
    Flubble *pflubble;
    pflubble = new Flubble();
    pflubble->print();
}
